import { createStore } from 'vuex';
import TodoItem from './../models/todoItem';
import TodoService from './../services/TodoService';


export default createStore({
  state: {
    todoList: new Array<TodoItem>()
  },
  mutations: {
    setTodos(state: any, todoList: TodoItem[]) {
      state.todoList.push(...todoList);
    },
    setTodoItem(state: any, todo: TodoItem) {
      state.todoList.push(todo);
    },
    setTodoItemDone(state: any, todo: TodoItem) {
      for (let index = 0; index < state.TodoItem; index++) {
        const element = state.TodoItem[index];
        
        if (element.task === todo.task) {
          element.done = !element.done;
        }
      }
    }
  },
  actions: {
    createTask({ commit, state }, todo: TodoItem) {
      commit("setTodoItem", todo);
    },
    loadTodo: ({ commit, state }) => {
      let service = new TodoService();
      commit("setTodos", service.load());
    },
    makeTodoDone({ commit, state }, todo: TodoItem) {
      todo.done = !todo.done;
      commit("setTodoItemDone", todo.done);
    }
  },
  modules: {
  }
})
