import TodoItem from "@/models/todoItem";

export default class TodoService{
    public load(){
      const todo1 = new TodoItem("Teste 1", false);
      const todo2 = new TodoItem("Teste 2", true);
      return [todo1, todo2];
    }
}